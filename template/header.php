<!DOCTYPE html>
<html>
<head lang="ru">
	<meta charset="UTF-8">
	<title>title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="/fonts/font.css">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/plugin/jquery/dist/jquery-ui-1.10.1.css">

	<link class="test-script-tmp" rel="stylesheet" type="text/less" href="css/main.less">
	<script class="test-script-tmp" type="text/javascript" src="/plugin/less/dist/less.min.js"></script>
	
</head>
<body>
<section class="header">
    Header
</section>