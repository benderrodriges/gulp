<script type="text/javascript" src="/plugin/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="/plugin/jquery/dist/jquery-ui-1.10.1.min.js"></script>

<script type="text/javascript" src="/js/helpers.js"></script>
<script src="/plugin/cookie/jquery.cookie.js"></script>

<!-- Forms -->
	<script type="text/javascript" src="/plugin/aSendForm/assets/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/plugin/aSendForm/assets/additional-methods.min.js"></script>
	<script type="text/javascript" src="/plugin/aSendForm/assets/jquery.bpopup.min.js"></script>
	<script type="text/javascript" src="/plugin/aSendForm/assets/jquery.send.form.js"></script>
	<link rel="stylesheet" type="text/css" href="/plugin/aSendForm/assets/a-valid.css">
<!-- Forms -->

<!-- Scroll -->
	<script type="text/javascript" src="/plugin/ScrollMagic/scrollmagic/minified/plugins/EasePack.min.js"></script>
	<script type="text/javascript" src="/plugin/ScrollMagic/scrollmagic/minified/plugins/TweenLite.min.js"></script>
	<script type="text/javascript" src="/plugin/ScrollMagic/scrollmagic/minified/ScrollMagic.min.js"></script>
	<script type="text/javascript" src="/plugin/ScrollMagic/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
	<script type="text/javascript" src="/plugin/ScrollMagic/scrollmagic/minified/plugins/ScrollToPlugin.min.js"></script>
	<script type="text/javascript" class="test-script-tmp" src="/plugin/ScrollMagic/scrollmagic/minified/plugins/debug.addIndicators.min.js"></script>
<!-- Scroll -->

<!-- CountDown -->
	<script type="text/javascript" src="/plugin/coundown/jquery.plugin.min.js"></script>
	<script type="text/javascript" src="/plugin/coundown/jquery.countdown.js"></script>
	<script type="text/javascript" src="/plugin/coundown/jquery.cookie.js"></script>
	<link rel="stylesheet" type="text/css" href="/plugin/coundown/jquery.countdown.css">
<!-- CountDown -->

<!-- Owl.carusel -->
<link rel="stylesheet" type="text/css" href="/plugin/owlCarousel2-2.2.1/dist/assets/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/plugin/owlCarousel2-2.2.1/dist/assets/owl.theme.default.min.css">
<script type="text/javascript" src="/plugin/owlCarousel2-2.2.1/dist/owl.carousel.min.js"></script>
<!-- Owl.carusel -->

<!-- gmap3 
<script src="http://maps.google.com/maps/api/js"></script>
<script src="/plugin/gmap3/dist/gmap3.min.js"></script>-->
<!-- gmap3 -->

<script type="text/javascript" src="/js/scriptsForAllPages.js"></script>