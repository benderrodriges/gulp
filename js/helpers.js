
offsetDef = - ($(window).height() * 0.2);

function animLoongHook(elem,elems,anim,times,stime) {
	$(elems).addClass("opacity-0");
	var controller = new ScrollMagic.Controller({globalSceneOptions: {},loglevel: 3});
	var sence = new ScrollMagic.Scene({triggerElement: $(elem)[0], offset: offsetDef})
		.addTo(controller)
		//.addIndicators()
		.on('start', function(e) {
			sence.remove();
			var time = (stime||0);
			$(elems).each(function(){
			if(!$(this).hasClass("animated")){
				var $this = $(this);
				setTimeout(function(){
					$this.removeClass("opacity-0");
					$this.addClass(anim+' animated');
				},time);
				setTimeout(function(){
					$this.removeClass(anim);
				},time+1000);
				time= time + times;
			}
		})
	});
}

function animStepHook(elem,anim,dealy,opa) {
	var controller = new ScrollMagic.Controller({globalSceneOptions: {},loglevel: 3});
	opa = (opa||false);
	dealy = (dealy||0);
	if (opa) {
		$(elem).addClass("opacity-0");
	};
	$(elem).each(function(){
		var $this = $(this);
		var sence = new ScrollMagic.Scene({triggerElement: $this[0], offset: offsetDef})
		.addTo(controller)
		//.addIndicators()
		.on('start', function(e) {
			sence.remove();
			setTimeout(function(){
				if (typeof anim != "function") {
					$this.addClass(anim+' animated');
				} else{
					anim();
				};
			},dealy);
		});
	})
}

function dedline(array_time,expir,selector,type) {
	selector = selector || '.countdown';
	type = type || 'DHMS';
	
    var austDay = new Date();
    austDay.setDate(austDay.getDate()+(7-austDay.getDay()));

    austDay.setSeconds(0);
	austDay.setMinutes(0);
	austDay.setHours(24);

	$(selector).countdown({until: austDay, padZeroes: true,format: type});
}

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       || 
          window.webkitRequestAnimationFrame || 
          window.mozRequestAnimationFrame    || 
          window.oRequestAnimationFrame      || 
          window.msRequestAnimationFrame     || 
          function(/* function */ callback, /* DOMElement */ element){
            window.setTimeout(callback, 1000 / 60);
          };
})();


// scrollController(new Array(
// 		["#sec1",".sec1"],
// 		["#sec2",".sec2"],
// 		["#sec3",".sec3"]
// 	));

function scrollController (linkObject) {
	if (typeof ScrollMagic !== "undefined"){
		var controller = new ScrollMagic.Controller({globalSceneOptions: {},loglevel: 3});
		
		var addSence = function(trigger,link) {
			var scene = new ScrollMagic.Scene({triggerElement: trigger, duration: ($(trigger).height())})
						.setClassToggle(link, "active")
						.addTo(controller);
		}

		for (var i = linkObject.length - 1; i >= 0; i--) {
			addSence(linkObject[i][0],linkObject[i][1]);
		};


		// change behaviour of controller to animate scroll instead of jump
		controller.scrollTo(function (newpos) {
			TweenLite.to(window, 0.5, {scrollTo: {y: newpos-60}});
		});

		//  bind scroll to anchor links
		$(document).on("click", "a[href^=#]", function (e) {
			var id = $(this).attr("href");
			if ($(id).length > 0) {
				e.preventDefault();

				// trigger scroll
				controller.scrollTo(id);

					// if supported by the browser we can even update the URL.
				if (window.history && window.history.pushState) {
					history.pushState("", document.title, id);
				}
			}
		});
	}
}

function getParameterByName (name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

function SmoothScroll (speed, dis) {
	var $window = $(window);
	
	var scrollTime = (speed|| 0.4);
	var scrollDistance = (dis|| $(window).height() * 0.3);
	var dataS = false;
	var pageH = $(document).height() - $(window).height();
	$(window).resize(function(){
		pageH = $(document).height() - $(window).height();
	})
	$window.on("mousewheel DOMMouseScroll", function(event){
		event.preventDefault();	
		if (dataS) {
			var delta = (event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3) * -1;
			scroll(dataS.vars.scrollTo.y + ((scrollDistance * delta) / 1.3));
		}else{
			var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
			var scrollTop = $window.scrollTop();
			scroll(scrollTop - parseInt(delta*scrollDistance));
		}
		
		if (dataS.vars.scrollTo.y < 0) {dataS.vars.scrollTo.y = 0}else
		if (dataS.vars.scrollTo.y > pageH) {dataS.vars.scrollTo.y = pageH};
	});
	var scroll = function(finalScroll){
		dataS = TweenLite.to($window, scrollTime, {
			scrollTo : { y: finalScroll, autoKill:true },
				ease: Power1.easeOut,	//For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
				autoKill: true,
				overwrite: 5,
				onComplete: function(){
					dataS.kill();
					dataS = false;
				}
			});

	}
}