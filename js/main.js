$(function(){

});


function forms() {
	var asoc = {
		// общие
			'name' : 'Имя',
			"surname" : "Фамилия",
			'phone' : 'Номер телефона',
			'email' : 'Е-mail',

			'wyn': 'Чего хочет клиент',
			"":""
		};
		
		var heandle = "/plugin/aSendForm/assets/handle.php";

		var vRules = {
				rules: {
					name: {
						required: true
					},
					surname: {
						required: true
					},
					email: {
						required: true,
						email: true
					},
					phone: {
						required: true,
						regex: /^([\(\)\+\- ]{0,2}[\d]){10,13}$/g
					},
					oblast: {
						required: true
					},
					town: {
						required: true
					},
					namberNP: {
						required: true
					}
				},

				messages: {
					name: {
						required: "Это поле обязательное для заполнения",
						email: "Введите данные в указаном формате"
					},
					email: {
						email: "Введите данные в указаном формате"
					},
					phone:{
						required: "Это поле обязательное для заполнения",
						regex : 'Номер нужно указывать в международном формате<br><span>+1 234 567 89 01</span>'
					}
				},
			};
			
		$(".header .container .header-need-consultation form").aSendForm({
			goal : function(){
				// ga('send', 'event', 'knopka', 'zakazat');
				// yaCounter26593851.reachGoal('send');
			},
			postQuery: heandle,
			closeData: 113000,
			associations : asoc,
			// mailTo : mail,
			//answer: true,
			validateRuls: vRules,
			onClickPopup: function(){
				
			},
		});

		$(".popUp-consltation form").aSendForm({
			popup : ['.popUp-consltation', '.block4 .container .block4-consultation button'],
			goal : function(){
				// ga('send', 'event', 'knopka', 'zakazat');
				// yaCounter26593851.reachGoal('send');
			},
			postQuery: heandle,
			closeData: 113000,
			associations : asoc,
			// mailTo : mail,
			//answer: true,
			validateRuls: vRules,
			onClickPopup: function(){
				
			},
		});
}